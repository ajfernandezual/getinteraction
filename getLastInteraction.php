

<?php

	//It is used as a service that create a file "lastinteraction.txt" so the "seeInteraction.php" can use the info of the last interaction.


	//Database Data Connection
	$servername="us-cdbr-azure-west-c.cloudapp.net";
	$username="bc479dc8ea36df";
	$password="b5e1ed07";
	$dbname="interactiondb";
	$conn = new mysqli($servername, $username, $password, $dbname);


	//query about interaction and user
	$result1 = mysqli_query($conn, 'SELECT idInteraction, operationPerformed, dateTime, users.name, users.surname, longitude, weatherDescription, weatherSubdescription, temperature, humidity, interactions.city, interactions.country, users.city as cityUser, users.country as countryUser
									FROM interactiondb.interactions, users
									WHERE users.idUserClient = interactions.Users_idUser 
									ORDER BY dateTime DESC
									LIMIT 0,1;'
							);
	$idInteraction=-1;
	while ( $row = mysqli_fetch_array($result1, MYSQLI_ASSOC) )
	{
		$json = json_encode($row);
		file_put_contents("lastinteraction.txt", $json);
		$idInteraction=$row["idInteraction"];
	}
	
	
	
	//query about action components
	$result1 = mysqli_query($conn, 'SELECT  actioncomponents.id, actioncomponents.interactions_idInteraction, services.name, services.category, services.section, services.subsection
									FROM interactiondb.actioncomponents, interactiondb.services
									WHERE interactiondb.actioncomponents.services_idServiceClient = interactiondb.services.idServiceClient AND  interactions_IdInteraction='. $idInteraction .';'
							);
	$array = array();
	while ( $row = mysqli_fetch_array($result1, MYSQLI_ASSOC) )
	{
		$array[] = $row;				
	}
	$json = json_encode($array);
	file_put_contents("lastaction.txt", $json);
	
	
	//query about group components
	$result1 = mysqli_query($conn, 'SELECT  groupcomponents.id, groupcomponents.interactions_idInteraction, services.name, services.category, services.section, services.subsection
									FROM interactiondb.groupcomponents, interactiondb.services
									WHERE interactiondb.groupcomponents.services_idServiceClient = interactiondb.services.idServiceClient AND  interactions_IdInteraction='. $idInteraction .';'
							);
	$array = array();
	while ( $row = mysqli_fetch_array($result1, MYSQLI_ASSOC) )
	{
		$array[] = $row;
	}
	$json = json_encode($array);
	file_put_contents("lastgroup.txt", $json);

	//query about ungroup components
	$result1 = mysqli_query($conn, 'SELECT  ungroupcomponents.id, ungroupcomponents.interactions_idInteraction, services.name, services.category, services.section, services.subsection
									FROM interactiondb.ungroupcomponents, interactiondb.services
									WHERE interactiondb.ungroupcomponents.services_idServiceClient = interactiondb.services.idServiceClient AND  interactions_IdInteraction='. $idInteraction .';'
							);
	$array = array();
	while ( $row = mysqli_fetch_array($result1, MYSQLI_ASSOC) )
	{
		$array[] = $row;
	}	
	$json = json_encode($array);
	file_put_contents("lastungroup.txt", $json);
	
	
	
	//query last interaction performed
	/*$result1 = mysqli_query($conn, 'SELECT idInteraction, operationPerformed, dateTime, users.name, users.surname, longitude, weatherDescription, weatherSubdescription, temperature, humidity, interactions.city, interactions.country, users.city as cityUser, users.country as countryUser
									FROM interactiondb.interactions, users
									WHERE users.idUserClient = interactions.Users_idUser 
									ORDER BY dateTime DESC
									LIMIT 0,1;');*/
	
	$consulta = "SELECT interactions.idInteraction
									   ,interactions.dateTime
									   ,interactions.operationPerformed
									   ,interactions.latitude
									   ,interactions.longitude
									   ,interactions.weatherDescription
									   ,interactions.temperature
									   ,interactions.country AS countryInteraction
									   ,interactions.city AS cityInteraction
									   ,users.idUserClient
									   ,users.name
									   ,users.surname
									   ,users.userType
									   ,users.userSubType
									   ,users.birthDate
									   ,users.city AS cityOrigin
									   ,users.country AS countryOrigin
									   ,users.email
									   ,sessions.deviceType
									   ,sessions.interactionType
									   ,actioncomponents.services_idServiceClient AS actionComponents
									   ,groupcomponents.services_idServiceClient AS groupComponents
									   ,ungroupcomponents.services_idServiceClient AS ungroupComponents
									FROM interactiondb.interactions
									INNER JOIN users ON users.idUserClient=interactions.Users_idUser
									INNER JOIN sessions ON sessions.idSession= interactions.Sessions_idSession
									INNER JOIN actioncomponents ON actioncomponents.interactions_idInteraction = interactions.idInteraction
									INNER JOIN groupcomponents ON groupcomponents.interactions_idInteraction = interactions.idInteraction
									INNER JOIN ungroupcomponents ON ungroupcomponents.interactions_idInteraction = interactions.idInteraction
									WHERE interactions.latitude NOT LIKE ('')
									ORDER BY datetime DESC
									LIMIT 0,1;";
	$result1 = mysqli_query($conn, $consulta);									
	$array = array();
	while ( $row = mysqli_fetch_array($result1, MYSQLI_ASSOC) )
	{
		$array[] = $row;
	}	
	$json = json_encode($array);
	file_put_contents("lastInteractionPerformed.txt", $json);
	
	
	//Decode the JSON and convert it into an associative array.
	$jsonDecoded = json_decode($json, true);
 
	//Give our CSV file a name.
	$csvFileName = 'lastInteractionPerformed.csv';
 
	//Open file pointer.
	$fp = fopen($csvFileName, 'w');
 
	//Loop through the associative array.
	foreach($jsonDecoded as $row)
	{
		//Write the row to the CSV file.
		fputcsv($fp, $row);
	}
 
	//Finally, close the file pointer.
	fclose($fp);
	



	//$lastInteraction = file_get_contents("lastinteraction.txt");
	//echo($lastInteraction);
?>