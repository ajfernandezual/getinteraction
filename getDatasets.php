<?php

header('Access-Control-Allow-Origin: *');  

//Database Data Connection
$servername="us-cdbr-azure-west-c.cloudapp.net";
$username="bc479dc8ea36df";
$password="b5e1ed07";
$dbname="interactiondb";
$conn = new mysqli($servername, $username, $password, $dbname);

$NumRows=-1;
$UserTypeArray=array();
$UserTypeValueArray=array();
$UserSubTypeArray=array();
$UserSubTypeValueArray=array();
$UserArray=array();
$UserValueArray=array();

//Check connection
if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error); } 

ob_start();

//Last 10.000 Add - AddGroup from all users
addAllUsers($conn);
//Last 10.000 Add - AddGroup from all users with weather condition
addAllUsersWeather($conn);						
//For each userType we generates a csv with ther 10.000 last Add-AddGroup	
addUserType($conn);	
//For each userSubType we generates a csv with ther 10.000 last Add-AddGroup	
addUserSubType($conn);	
//For each user we generates a csv with ther 10.000 last Add-AddGroup	
addUser($conn);	

//cerrar conexión
$conn->close();

$text = ob_get_contents();
ob_end_clean();








function addAllUsers($conn)
{
	echo("Generating csv file ......: addAllUsers <br>");
	//query from the database 
	$result = mysqli_query($conn, 'SELECT idInteraction, operationPerformed, dateTime, interactions.Users_idUser, name, surname 
								FROM interactiondb.interactions, users
								WHERE (operationPerformed = "Add" OR operationPerformed = "AddGroup") AND (users.idUserClient = interactions.Users_idUser)
								ORDER  BY  (dateTime) DESC
								LIMIT 0, 10000;'
						);
	$headerArray = array('idInteraction', 'OperationPerformed', 'dateTime', 'UserId', 'name', 'surname');
	createCSV ($result, "1.addAllUsers", $headerArray);
	//global $NumRows;
	//$NumRows	= $result->num_rows;
	$GLOBALS['NumRows']=$result->num_rows;
}


function addAllUsersWeather($conn)
{
	echo("Generating csv file ......: addAllUsersWeather <br>");
	//query from the database 
	$result = mysqli_query($conn, 'SELECT idInteraction, operationPerformed, dateTime, interactions.Users_idUser, name, surname, latitude, longitude, weatherDescription, weatherSubdescription, temperature, pressure, humidity, cloudPercentage, windSpeed, windDirection, interactions.city, interactions.country 
								FROM interactiondb.interactions, users
								WHERE (operationPerformed = "Add" OR operationPerformed = "AddGroup") AND (users.idUserClient = interactions.Users_idUser)
								ORDER  BY  (dateTime) DESC
								LIMIT 0, 10000;'
						);
	$headerArray = array('idInteraction', 'OperationPerformed', 'dateTime', 'UserId', 'name', 'surname', 'latitude', 'longitude', 'weatherDescription', 'weatherSubdescription', 'temperature', 'pressure', 'humidity', 'cloudPercentage', 'windSpeed', 'windDirection', 'city', 'country');
	
	
	createCSV ($result, "2.addAllUsersWeather", $headerArray);
}


function addUserType($conn)
{
	//Query to know all the userType that exists
	$result = mysqli_query($conn, 'SELECT DISTINCT userType FROM interactiondb.users GROUP by userType;');
	//for each usesrType we generates a csv with ther 10.000 last Add-AddGroup
	while($row = $result->fetch_assoc()) 
	{
		echo("Generating csv file userType......: " . $row["userType"] . "<br>");
		//query from the database 
		$result2 = mysqli_query($conn, 'SELECT idInteraction, operationPerformed, dateTime, interactions.Users_idUser, name, surname
									FROM interactiondb.interactions, users
									WHERE (operationPerformed = "Add" OR operationPerformed = "AddGroup") AND (users.idUserClient = interactions.Users_idUser) AND (users.userType = "' . $row["userType"] . '") 
									ORDER  BY  (dateTime) DESC
									LIMIT 0, 10000;'
								);
		$headerArray = array('idInteraction', 'OperationPerformed', 'dateTime', 'UserId', 'name', 'surname');
		createCSV ($result2, "3.addUserType".$row["userType"], $headerArray);
		//array for graphic visualization
		array_push($GLOBALS['UserTypeArray'], $row["userType"]);
		array_push($GLOBALS['UserTypeValueArray'], $result2->num_rows);
	}
}


function addUserSubType($conn)
{
	//Query to know all the userSubType that exists
	$result = mysqli_query($conn, 'SELECT DISTINCT userSubType FROM interactiondb.users GROUP by userSubType;');
	//for each usesrType we generates a csv with ther 10.000 last Add-AddGroup
	while($row = $result->fetch_assoc()) 
	{
		echo("Generating csv file userSubType......: " . $row["userSubType"] . "<br>");
		//query from the database 
		$result2 = mysqli_query($conn, 'SELECT idInteraction, operationPerformed, dateTime, interactions.Users_idUser, name, surname
									FROM interactiondb.interactions, users
									WHERE (operationPerformed = "Add" OR operationPerformed = "AddGroup") AND (users.idUserClient = interactions.Users_idUser) AND (users.userSubType = "' . $row["userSubType"] . '") 
									ORDER  BY  (dateTime) DESC
									LIMIT 0, 10000;'
								);
		$headerArray = array('idInteraction', 'OperationPerformed', 'dateTime', 'UserId', 'name', 'surname');
		createCSV ($result2, "4.addUserSubType".$row["userSubType"], $headerArray);
		//array for graphic visualization
		array_push($GLOBALS['UserSubTypeArray'], $row["userSubType"]);
		array_push($GLOBALS['UserSubTypeValueArray'], $result2->num_rows);
	}
}


function addUser($conn)
{
	//Query to know all the userType that exists
	$result = mysqli_query($conn, 'SELECT DISTINCT idUserClient FROM interactiondb.users GROUP by idUserClient;');
	//for each user we generates a csv with ther 10.000 last Add-AddGroup
	while($row = $result->fetch_assoc()) 
	{
		echo("Generating csv file users......: " . $row["idUserClient"] . "<br>");
		//query from the database 
		$result2 = mysqli_query($conn, 'SELECT idInteraction, operationPerformed, dateTime, interactions.Users_idUser, name, surname
									FROM interactiondb.interactions, users
									WHERE (operationPerformed = "Add" OR operationPerformed = "AddGroup") AND (users.idUserClient = interactions.Users_idUser) AND (users.idUserClient = "' . $row["idUserClient"] . '") 
									ORDER  BY  (dateTime) DESC
									LIMIT 0, 10000;'
								);
		$headerArray = array('idInteraction', 'OperationPerformed', 'dateTime', 'UserId', 'name', 'surname');
		createCSV ($result2, "5.addUser".$row["idUserClient"], $headerArray);
		//array for graphic visualization
		array_push($GLOBALS['UserArray'], $row["idUserClient"]);
		array_push($GLOBALS['UserValueArray'], $result2->num_rows);

	}
}


function createCSV($result, $csvFileName, $headerArray)
{
	//Delete the file if it exists
	unlink('appdata/' . $csvFileName . '.csv');
	
	
	//Open or create csv file
	$fp = fopen('appdata/' . $csvFileName . '.csv', 'w');
	fputcsv($fp, $headerArray);
	
	//screen info
	echo('appdata/' . $csvFileName);
	echo ("<br>");
	var_dump($headerArray);
	echo ("<br>");

	//add information of the query result in the csv file
	while ( $row = mysqli_fetch_array($result, MYSQLI_ASSOC) )
	{
		fputcsv($fp, $row);
		var_dump($row);
		echo ("<br>");
	}

	// Close csv file
	fclose($fp);
	echo ("<br><br>");
}




?>

	






<!DOCTYPE html>
<html>
	<head>
		<meta charset = 'utf-8'>
		<title>Get Datasets</title>
		<!--Import Google Icon Font-->
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!--Import materialize.css-->
		<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
		<!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<!-- jQuery -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	</head>
	<body>
	
	<center><h2> Add and AddGroup Operations Performed in ENIA User Interface </h2></center>
	<br>
	
		<div class = 'll'>
		
			<h3> All Users Interactions </h3>
		
			<div class="col s12 m8 offset-m2 l6 offset-l3">
				<div class="card-panel grey lighten-5 z-depth-1" style="width:calc(50% - 40px)">
				  <div class="row valign-wrapper" >
					<div class="col s2">
					  <img src="img/touchpoint-512.png" alt="" class="circle responsive-img">
					</div>
					<div class="col s10">
					  <span class="black-text">
						<p><?php echo($GLOBALS['NumRows']); ?> interactions</p> 
						Has been generated from all users.
					  </span>
					</div>
				  </div>
				</div>
				
				<div class="card-panel grey lighten-5 z-depth-1" style="width:calc(50% - 40px)">
				  <div class="row valign-wrapper" >
					<div class="col s2">
					  <img src="img/Weather_icons_grey-03-512.png" alt="" class="circle responsive-img">
					</div>
					<div class="col s10">
					  <span class="black-text">
						<p><?php echo($GLOBALS['NumRows']); ?> interactions</p> 
						Has been generated from all users including weather information..
					  </span>
					</div>
				  </div>
				</div>
			</div>	
				
		</div>	  
	

		<div class = 'll'>
		  <br>
		  <h3> User Type Interactions </h3>
		  
		  
		  
		  <?php  
		  

				for ($i=0; $i<count($GLOBALS['UserTypeValueArray']); $i++)
				{
					echo (' 
					<div class="col s12 m8 offset-m2 l6 offset-l3">
						<div class="card-panel grey lighten-5 z-depth-1" style="width:30%">
						  <div class="row valign-wrapper" >
							<div class="col s2">
							  <img src="img/IconCategory.png" alt="" class="circle responsive-img">
							</div>
							<div class="col s10">
							  <span class="black-text">
								<p>' .  $GLOBALS['UserTypeValueArray'][$i]  .' interactions</p> 
								Has been generated from users type: <strong>' .  $GLOBALS['UserTypeArray'][$i]  .' </strong>
							  </span>
							</div>
						  </div>
						</div>
					</div>
					
					');
				}
		  ?>
	  
		</div>
		
		
		
		<div class = 'll'>
		  <br>
		  <h3> User SubType Interactions </h3>
		  
		  
		  
		  <?php  
		  
				for ($i=0; $i<count($GLOBALS['UserSubTypeValueArray']); $i++)
				{
					echo (' 
					<div class="col s12 m8 offset-m2 l6 offset-l3">
						<div class="card-panel grey lighten-5 z-depth-1" style="width:30%">
						  <div class="row valign-wrapper" >
							<div class="col s2">
							  <img src="img/subcat.png" alt="" class="circle responsive-img">
							</div>
							<div class="col s10">
							  <span class="black-text">
								<p>'. $GLOBALS['UserSubTypeValueArray'][$i]  .' interactions</p> 
									Has been generated from users Subtype: <strong>' .  $GLOBALS['UserSubTypeArray'][$i]   .' </strong>
							  </span>
							</div>
						  </div>
						</div>
					</div>
					
					');
				}
		  ?>
	  
		</div>

		
		
		
		<div class = 'll'>
		  <br>
		  <h3> User Interactions </h3>
		  
		  
		  
		  <?php  
				$conn = new mysqli($servername, $username, $password, $dbname);
				for ($i=0; $i<count($GLOBALS['UserValueArray']); $i++)
				{
					$result = mysqli_query($conn, 'SELECT CONCAT(name, " ", surname) AS namesurname FROM interactiondb.users WHERE idUserClient='. $GLOBALS['UserArray'][$i].';');
						
					//echo('SELECT CONCAT(name, " ", surname) AS namesurname FROM interactiondb.users WHERE idUserClient='. $GLOBALS['UserArray'][$i].';');
					$UserName="a";
					while($row = $result->fetch_assoc()) 
					{
						$UserName=$row["namesurname"];
					}
					
					
					echo (' 
					<div class="col s12 m8 offset-m2 l6 offset-l3">
						<div class="card-panel grey lighten-5 z-depth-1" style="width:22%">
						  <div class="row valign-wrapper" >
							<div class="col s2">
							  <img src="img/boy-512.png" alt="" class="circle responsive-img">
							</div>
							<div class="col s10">
							  <span class="black-text">
								<p>'. $GLOBALS['UserValueArray'][$i]  .' interactions</p> 
								Has been generated from user: <br>	<strong>' .  $UserName  .' </strong>
							  </span>
							</div>
						  </div>
						</div>
					</div>
					
					');
				}
				$conn->close();
		  ?>
	  
		</div>

		
		
		<!--
		 <div class="row">
			<form class="col s12">
			  <div class="row">
				<div class="input-field col s12">
				  <textarea id="textarea1" class="materialize-textarea"><?php echo("$text"); ?></textarea>
				  <label for="icon_prefix2">Interaction Log</label>
				</div>
			  </div>
			</form>
		  </div>
		  
		  
		  <div class="row">
			<form class="col s12">
			  <div class="row">
				<div class="input-field col s12">
				  <i class="material-icons prefix">mode_edit</i>
				  <textarea id="icon_prefix2" class="materialize-textarea" style="height:500px; overflow-y: scroll; margin-top:20px;" ><?php echo("$text"); ?></textarea>
				  <label for="icon_prefix2">Interaction Log</label>
				</div>
			  </div>
			</form>
		  </div>
		  -->

	</body>
</html>





<style type='text/css'>
	.black-text p{
		font-size:30px; 
		margin-bottom: 0px; 
		margin-top: 0px;
	}
	
	.card-panel
	{
		float:left;
		margin: 20px;
	}
	
	.ll{
		float:left;
		width:100%;
	}
	
	h3{
		margin-left: 20px !important;
		color: gray !important;
	}
</style>




        

<?php
echo($text);
?>