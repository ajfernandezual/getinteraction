

SET SQL_SAFE_UPDATES=0;
DELETE FROM interactiondb.actioncomponents;
DELETE FROM interactiondb.groupcomponents;
DELETE FROM interactiondb.ungroupcomponents;
DELETE FROM interactiondb.components;
DELETE FROM interactiondb.cotsgets;
DELETE FROM interactiondb.interactions;
DELETE FROM interactiondb.users;
DELETE FROM interactiondb.services;
DELETE FROM interactiondb.sessions;
SET SQL_SAFE_UPDATES=1;