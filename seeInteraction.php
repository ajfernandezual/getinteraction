
<?php


// create a new cURL resource
$ch = curl_init();

// set URL and other appropriate options
curl_setopt($ch, CURLOPT_URL, "http://getinteraction.azurewebsites.net/getLastInteraction.php");
curl_setopt($ch, CURLOPT_HEADER, 0);

// grab URL and pass it to the browser
$result =curl_exec($ch);

// close cURL resource, and free up system resources
curl_close($ch);


// Abre el fichero para obtener el contenido existente
$lastinteraction = file_get_contents("lastinteraction.txt");
$jsoninteraction = json_decode ($lastinteraction);

$lastaction = file_get_contents("lastaction.txt");
$jsonaction = json_decode ($lastaction);

$lastgroup = file_get_contents("lastgroup.txt");
$jsongroup = json_decode ($lastgroup);

$lastungroup = file_get_contents("lastungroup.txt");
$jsonungroup = json_decode ($lastungroup);


/*$htmlJsonAction = "";
foreach ($jsonaction as $i=>$item) 
				{
					$htmlJsonAction.='
						<div class="input-field col s12">
							<input disabled value="'.$item->name.'"  type="text" class="validate" id="c'.$item->id.'">	
							<label for="disabled" class="active">Component ' . ($i+1) .' </label>
						</div>';
				}
$htmlJsonGroups = "";
foreach ($jsongroup as $i=>$item) 
				{
					$htmlJsonGroups.='
						<div class="input-field col s12">
							<input disabled value="'.$item->name.'"  type="text" class="validate" id="c'.$item->id.'">	
							<label for="disabled" class="active"> Component ' . ($i+1) .' </label>
						</div>';
				}
$htmlJsonUngroups = "";
foreach ($jsonungroup as $i=>$item) 
				{
					$htmlJsonUngroups.='
						<div class="input-field col s12">
							<input disabled value="'.$item->name.'"  type="text" class="validate" id="c'.$item->id.'">	
							<label for="disabled" class="active">Component ' . ($i+1) .' </label>
						</div>';
				}				
				
*/			
				
$htmlJsonAction = "";
foreach ($jsonaction as $i=>$item) 
{
	$htmlJsonAction.='
	<div class="input-field col s12">
		<input disabled value="'.$item->name.'"  type="text" class="validate" id="c'.$item->id.'">	
	</div>';
}
$htmlJsonGroups = "";
foreach ($jsongroup as $i=>$item) 
{
	$htmlJsonGroups.='
	<div class="input-field col s12">
		<input disabled value="'.$item->name.'"  type="text" class="validate" id="c'.$item->id.'">	
	</div>';
}
$htmlJsonUngroups = "";
foreach ($jsonungroup as $i=>$item) 
{
	$htmlJsonUngroups.='
	<div class="input-field col s12">
		<input disabled value="'.$item->name.'"  type="text" class="validate" id="c'.$item->id.'">	
	</div>';
}					
				
if (isset($_GET["onlyData"]))
{
	$data = array(
		"lastinteraction"=>$jsoninteraction,
		"actions"=>$htmlJsonAction,
		"groups"=>$htmlJsonGroups,
		"ungroups"=>$htmlJsonUngroups		
	);
	
	echo json_encode($data);
	die;
}

?>



<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<title>Interaction Viewer</title>
		<!--Import Google Icon Font-->
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!--Import materialize.css-->
		<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
		<!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<!-- jQuery -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	</head>


	<body >
		<!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
	
	
		<object data=http://150.214.150.163/enia/ui/ width="80%" style="float:left; height:calc(100% - 10px);"> 
			<embed src=http://150.214.150.163/enia/ui/ width="80%" style="float:left; height:calc(100% - 10px);"> 
			</embed> Error: Embedded data could not be displayed. 
		</object>

		
	<div style="width:20%; float:right; height:calc(100% - 10px); overflow-y:scroll;">
		
		<div class="row" style="width:100%; ">

			<a class="waves-effect waves-light btn">Interaction Info</a>	

				<div class="input-field col s12">
					<input disabled value=<?php echo($jsoninteraction->idInteraction); ?> type="text" class="validate" id='inputIdInteraction'>	
					<!--<input disabled value=<?php echo('idInteraction:&nbsp;'.$jsoninteraction->idInteraction); ?> type="text" class="validate" id='inputIdInteraction'>	-->
				
				</div>
			     
				<div class="input-field col s12">
					<input disabled value=<?php echo($jsoninteraction->operationPerformed); ?> type="text" class="validate" id='inputOperationPerformed'>	
					<!--<label for="inputOperationPerformed">Operation Performed</label>-->
				</div>
				
				<div class="input-field col s12">
					<input disabled value=<?php echo($jsoninteraction->dateTime); ?>  type="text" class="validate" id='inputDateTime'>	
					<!--<label for="disabled">Date Time</label>-->
				</div>
				
				<div class="input-field col s12">
					<input disabled value=<?php echo($jsoninteraction->city); ?>  type="text" class="validate" id='inputCity'>	
					<!--<label for="disabled">City</label>-->
				</div>
				  
				<div class="input-field col s12">
					<input disabled value=<?php echo($jsoninteraction->country); ?>  type="text" class="validate" id='inputCountry'>	
					<!--<label for="disabled">Country</label>-->
				</div>
				  
				
				
			
			
			<a class="waves-effect waves-light btn">User Info</a>
			
						     
				<div class="input-field col s12">
					
					<input disabled value="<?php echo($jsoninteraction->name);?> <?php echo($jsoninteraction->surname);?>"  type="text" class="validate" id='inputName'>	
					<!--<label for="inputOperationPerformed">Name</label>-->
				</div>
				
				<div class="input-field col s12">
					<input disabled value=<?php echo($jsoninteraction->cityUser); ?>  type="text" class="validate" id='inputUserCity'>	
					<!--<label for="disabled">City</label>-->
				</div>
				
				<div class="input-field col s12">
					<input disabled value=<?php echo($jsoninteraction->countryUser); ?>  type="text" class="validate" id='inputUserCountry'>	
					<!--<label for="disabled">Country</label>-->
				</div>				
				  
			

				  
		
			<a class="waves-effect waves-light btn">Action Components</a>
			
				<div id = 'actions'>
				<?php
					echo $htmlJsonAction;
				?>
				</div>
			
			
			<a class="waves-effect waves-light btn">Group Components</a>
			
				<div id = 'groups'>
				<?php
					echo $htmlJsonAction;
				?>
				</div>
			
			<a class="waves-effect waves-light btn">Ungroup Components</a>
			
				<div id = 'ungroups'>
				<?php
					echo $htmlJsonAction;
				?>
				</div>
			
			
			<a class="waves-effect waves-light btn">Weather Info</a>
			
				<div class="input-field col s12">
					<input disabled value=<?php echo($jsoninteraction->weatherDescription); ?>  type="text" class="validate" id='inputWeatherDescription'>	
					<!--<label for="disabled">Weather Description</label>-->
				</div>				  
				  
				<div class="input-field col s12">
					<input disabled value=<?php echo($jsoninteraction->weatherSubdescription); ?>  type="text" class="validate" id='inputWeatherSubdescription'>	
					<!--<label for="disabled">Weather Subdescription</label>-->
				</div>			
			
				<div class="input-field col s12">
					<input disabled value=<?php echo($jsoninteraction->temperature); ?>º&nbsp;(Celsius) type="text" class="validate" id='inputTemperature'>	
					<!--<label for="disabled">Temperature</label>-->
				</div>				  
				  
				<div class="input-field col s12">
					<input disabled value=<?php echo($jsoninteraction->humidity); ?>%&nbsp;(Pressure)  type="text" class="validate" id='inputHumidity'>	
					<!--<label for="disabled">Humidity</label>-->
				</div>
				  

			
		</div>
	  
    </div>
		
		
		
	</body>
</html>


<style type='text/css'>
	.row a{
		width:100%;
		text-align:left;
	}
</style>

<style type='text/css'>
	.row input{
		width:100%;
		text-align:left;
		color:black !important;
		margin-top:-14px;
		margin-bottom:-4px;
	}
</style>

<script type="text/javascript">

	$(document).ready(function(){
		setInterval(function(){
			$.ajax({
				  url: "seeInteraction.php",
				  data:{onlyData:1}
				}).done(function(data) {
					console.debug(JSON.parse(data));
					
					var data = JSON.parse(data);	
					$("#inputIdInteraction").val(data.lastinteraction.idInteraction);
					$("#inputOperationPerformed").val(data.lastinteraction.operationPerformed);
					$("#inputDateTime").val(data.lastinteraction.dateTime);
					$("#inputCity").val(data.lastinteraction.city);
					$("#inputCountry").val(data.lastinteraction.country);
					$("#inputName").val(data.lastinteraction.name+' '+data.lastinteraction.surname);
					//$("#inputSurname").val(data.lastinteraction.surname);
					$("#inputUserCity").val(data.lastinteraction.cityUser);
					$("#inputUserCountry").val(data.lastinteraction.countryUser);
					$("#inputWeatherDescription").val(data.lastinteraction.weatherDescription);
					$("#inputWeatherSubdescription").val(data.lastinteraction.weatherSubdescription);
					$("#inputTemperature").val(data.lastinteraction.temperature+'º (Celsius)');
					$("#inputHumidity").val(data.lastinteraction.humidity+'% (Pressure)');
					$("#actions").html(data.actions);
					$("#groups").html(data.groups);
					$("#ungroups").html(data.ungroups);
					
				});
		}, 3000)	
	})

</script>


    