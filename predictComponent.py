import urllib.request
import json

data = {
        "Inputs": {
                "input1":
                [
                    {
                            'idInteraction': "594231",   
                            'dateTime': "2017-04-04T06:27:29Z",   
                            'operationPerformed': "Add",   
                            'latitude': "36.8381",   
                            'longitude': "-2.4597",   
                            'weatherDescription': "Clear",   
                            'temperature': "11",   
                            'countryInteraction': "ES",   
                            'cityInteraction': "Almeria",   
                            'idUserClient': "100",   
                            'name': "Marta",   
                            'surname': "de Torres González",   
                            'userType': "Turista",   
                            'userSubType': "Turista",   
                            'birthDate': "1999-11-18",   
                            'cityOrigin': "Almería",   
                            'countryOrigin': "Spain",   
                            'email': "martadetorresg@gmail.com",   
                            'deviceType': "Browser",   
                            'interactionType': "NULL",   
                            'actionComponents': "http://acg.ual.es/wookie/widgets/OGC-RENPA-ZEPIM",   
                            'groupComponents': "",   
                            'ungroupComponents': "",   
                    }
                ],
        },
    "GlobalParameters":  {
    }
}

body = str.encode(json.dumps(data))

url = 'https://ussouthcentral.services.azureml.net/workspaces/e30ef7cf6301413aace9b250890956bf/services/523069a699524435adc9c4a65e476a37/execute?api-version=2.0&format=swagger'
api_key = 'a6yxO49PBJroKB5ciIln7FuuoYc+nm6iBWII7so3yVUiPErPhf3uFw0vi0uUUejCUifvsVOI1ufa2pdyvl/JJQ==' # Replace this with the API key for the web service
headers = {'Content-Type':'application/json', 'Authorization':('Bearer '+ api_key)}

req = urllib.request.Request(url, body, headers)

try:
    response = urllib.request.urlopen(req)

    result = response.read()
    print(result)
except urllib.error.HTTPError as error:
    print("The request failed with status code: " + str(error.code))

    # Print the headers - they include the requert ID and the timestamp, which are useful for debugging the failure
    print(error.info())
    print(json.loads(error.read().decode("utf8", 'ignore')))